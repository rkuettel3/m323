//Beispiel 1
function foo(params) {
    var name = params[0];
    var age = params[1];
    var args = params.slice(2);
    // any logic
}

foo(["name", 16, "other args"])

//Beispiel 2
function foo(name, age) {
    // any logic
}
foo("name", 16)

//Beispiel 3
function foo({name,age}) { //Destructuring
    // any logic
}
foo({name:"name", age:"age"})

//Beispiel 4
function foo(name, ...data) {
    console.log(`name:${name}, age:${data[0]}`)
    // any logic
}
foo("name", "age")