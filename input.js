/*
* Funktionen in der funktionalen Programmierung werden wie in der Mathematik
* als Einheit ohne globalen/versteckten State behandelt. Das bedeutet das die
* gleichen Eingaben immer zum selben Resultat führen werden.
* */

//funktion mit standartwert
function defaultWert(x = 1){
    console.log(x * 2);
}

defaultWert() // logs 2
defaultWert(0) // logs 0


// wie viele argumente erwarted die funktion
function noArgs(){}
function twoArgs(x, y){}

console.log(noArgs.length) // 0
console.log(twoArgs.length) //  2
console.log(defaultWert.length) // argumente mit standartwert werden nicht gezählt

// was für argumente wurden mitgegeben
function mitgegebeneArgumente(x, y, z){
    console.log(arguments)
}

mitgegebeneArgumente(1, 2, NaN) // logs array with all parameters and their values
mitgegebeneArgumente() // logs empty array


//variadic arguments
function manyArgs(...args){
    args.forEach(x=>console.log(x))
}
manyArgs(1,4,2,45,6)

function manyArgs2(){
    console.log(arguments)
}

manyArgs2(1,4,6) //arguments will still be saved in the arguments array