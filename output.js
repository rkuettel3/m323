function add() {}

function sub() {
    return;
}

function tree() {
    return undefined;
}

add() //undefined
sub() //undefined
tree() //undefined


function calc(x) {
    if (x > 10) return x + 10;
    var y = x / 2;
    if (y < 5) {
        if (x % 2 == 0) return x;
    }
    if (y < 10) return y;
    return x;
}

calc(5) //2.5
calc(20) //30

function calc2(x) {
    var result;

    if (x > 10) {
        result = x + 10;
    } else {
        var y = x / 2;
        if (y < 5 && x % 2 === 0) {
            result = x;
        } else if (y < 10) {
            result = y;
        } else {
            result = x;
        }
    }
    return result;
}


function sum(listOfNumbers) {
    return listOfNumbers.reduce((acc, num) => {
        const value = num || 0;
        return acc + value;
    }, 0);
}